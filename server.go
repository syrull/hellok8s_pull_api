package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis/v8"
)

var redisUrl = os.Getenv("REDIS_URL")

func CreateRedisClient() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     redisUrl,
		Password: "", // no password set
		DB:       0,  // use default DB
	})
}

func pull(c *gin.Context) {
	// redis := CreateRedisClient()
	c.JSON(http.StatusOK, gin.H{"status": "To be Implemented"})
}

func main() {
	r := gin.Default()
	r.GET("/pull/", pull)
	r.Run(":8080")
}
