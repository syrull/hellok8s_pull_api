# Hellok8s Pull API

This project is part of a learning plan about kubernetes and helm. The project has `helmcharts` and `k8s` folders, those are corresponding to kubernetes and helm.

# Building the Image

Make sure to run `minikube start --vm-driver=none`, so that it will use your current docker env, if that is not the case you can run:

```bash
$ eval $(minikube docker-env)
```

This command would export the needed env variables so you can use the docker that minikube uses. Then you can build the image:

```bash
$ docker build . -t hellok8s-app
```

# Deploying the application with Kubernetes

You can use the bash scripts for creating/deleting (`create.sh/delete.sh`) the Kubernetes resources. After running the scripts, the application should be alerady deployed on your k8s cluster.

# Deploying the application with Helm

TBW